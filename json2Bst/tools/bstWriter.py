import pathlib
import constants as cst

from collections import OrderedDict
from .bstDumper import bst_dump


class BstWriter:
    """Takes an element/ and writes to specified file"""

    yaml_node = None
    buildtool = ""

    def write_to_bst(self, file_path, module):
        """Converts class attributes into dicts for bstDumper"""

        # ensure we are working with a clean object
        self.yaml_node = {}

        path = pathlib.Path(file_path)

        if not path.exists():
            path.mkdir(parents=True, exist_ok=True)

        self.buildtool = module.buildtool()

        self.convert_module(module)

        if self.yaml_node:
            bst_dump(self.yaml_node, str(path.joinpath(module.name + '.bst')))

    def convert_module(self, module):
        """Parse an autotools module into valid yaml"""

        self.yaml_node[cst.BST_BUILDTOOLS[self.buildtool]
                       ['kind']] = self.buildtool

        self.yaml_node[cst.BST_BUILDTOOLS[self.buildtool]['description']
                       ] = 'json2bst conversion of {0}'.format(module.name)

        if module.variables:
            self.yaml_node[cst.BST_BUILDTOOLS[self.buildtool]
                           ['variables']] = self.convert_variables(module)

        if module.config:
            config = self.convert_config_options(module)

            if config:
                self.yaml_node[cst.BST_BUILDTOOLS[self.buildtool]
                               ['config']] = config

        if module.depends:
            self.yaml_node[cst.BST_BUILDTOOLS[self.buildtool]
                           ['depends']] = self.convert_dependencies(module)

        if module.sources:
            self.yaml_node[cst.BST_BUILDTOOLS[self.buildtool]
                           ['sources']] = self.convert_sources(module)

    def convert_sources(self, module):
        """Converts self.sources into a list of dicts for bstDumper"""

        sources = []

        for source in module.sources:

            # ensure we catch unspported source types for conversion
            if source.incompatable:
                self.convert_invalid_source(source)
            else:
                if source.kind() != 'tar':
                    source_obj = {'kind': source.kind()}
                else:
                    source_obj = {'kind': self.convert_archive(source)}

                for key in source.attributes.keys():
                    if key in source.incompatable:
                        print(
                            'Invalid key {0} found in {1}, skipping..'.format(
                                key, module.name))

                    source_obj[key] = source.attributes[key]

                sources.append(source_obj)

        return sources

    def convert_archive(self, source):
        """Ensure correct archive type is extracted"""

        for key in source.attributes.keys():
            if key == cst.SOURCE_PROPERTIES['url']:
                extension = pathlib.Path(source.attributes[key]).suffix[1:]

                if extension not in ['bzr', 'tar', 'zip']:
                    return 'tar'

                return extension

    def convert_invalid_source(self, source):
        """Takes an invalid source and tries to convert to valid bst"""

        if source.kind() == 'script':
            for key in source.attributes.keys():
                if key == 'commands':
                    for command in source.attributes[key]:
                        if 'autoreconf' in command.lower():
                            pass

    def convert_dependencies(self, module):
        """Parses module dependencies stored in 'module' based utilised
           buildtool"""

        dependencies = []

        dependencies = ['{0}.bst'.format(dep.name)for dep in module.depends]

        return dependencies

    def convert_config_options(self, module):
        """Overrides module config options with any parsed arguments"""

        conf_options = {}

        for key in module.config.keys():

            if key == 'build-commands':
                # pass for now until correct location
                # can be deciced
                pass

        return conf_options

    def convert_variables(self, module):

        variables = {}

        if self.buildtool == 'autotools':
            for key in module.variables.keys():

                if key == 'config-opts':

                    variables['conf-local'] = ''

                    for command in module.variables[key]:
                        variables['conf-local'] += (' ' + command)

        if self.buildtool == 'meson':
            for key in module.variables.keys():

                if key == 'config-opts':

                    variables['meson-local'] = ''

                    for command in module.variables[key]:
                        variables['meson-local'] += (' ' + command)

        if self.buildtool == 'cmake':
            for key in module.variables.keys():

                if key == 'config-opts':

                    variables['cmake-local'] = ''

                    for command in module.variables[key]:
                        variables['cmake-local'] += (' ' + command)

        return variables
